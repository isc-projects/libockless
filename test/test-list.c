#include <assert.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <pthread.h>
#include <threads.h>
#include <stdio.h>

#include <lockless/list.h>

#define NELEMENTS   2570
#define NTHREADS    128

static atomic_uint_fast32_t deletes = 0;
static atomic_uint_fast32_t inserts = 0;

static atomic_uintptr_t elements[NTHREADS][NELEMENTS] = { 0 };

#define TID_UNKNOWN -1

static atomic_int_fast32_t tid_i_base = ATOMIC_VAR_INIT(0);
static atomic_int_fast32_t tid_d_base = ATOMIC_VAR_INIT(0);

static thread_local int tid_i_v = TID_UNKNOWN;
static thread_local int tid_d_v = TID_UNKNOWN;

static inline int
tid_i(void) {
	if (tid_i_v == TID_UNKNOWN) {
		tid_i_v = atomic_fetch_add(&tid_i_base, 1);
		assert(tid_i_v < NTHREADS);
	}

	return (tid_i_v);
}

static inline int
tid_d(void) {
	if (tid_d_v == TID_UNKNOWN) {
		tid_d_v = atomic_fetch_add(&tid_d_base, 1);
		assert(tid_d_v < NTHREADS);
	}

	return (tid_d_v);
}

static void *
insert_thread(void *arg) {
	ll_list_t *list = (ll_list_t *)arg;

	for (size_t i = 0; i < NELEMENTS; i++) {
		atomic_uintptr_t *key = (atomic_uintptr_t *)&elements[tid_i()][i];
		if (ll_list_insert(list, (uintptr_t)key)) {
			atomic_fetch_add(key, 1);
			(void)atomic_fetch_add(&inserts, 1);
		}
	}
	return NULL;
}

static void *
delete_thread(void *arg) {
	ll_list_t *list = (ll_list_t *)arg;

	for (size_t i = 0; i < NELEMENTS; i++) {
		atomic_uintptr_t *key = (atomic_uintptr_t *)&elements[tid_d()][i];
		if (ll_list_delete(list, (uintptr_t)key)) {
			atomic_fetch_sub(key, 1);
			(void)atomic_fetch_add(&deletes, 1);
		}
	}
	return NULL;
}

enum what {
	INSERTS = 0x01,
	DELETES = 0x02,
	BOTH    = 0x03,
};

__attribute__((unused)) static void
run_threads(ll_list_t *list, size_t nthreads, int what) {
	pthread_t threads[nthreads];

	atomic_store(&tid_i_base, 0);
	atomic_store(&tid_d_base, 0);

	assert(nthreads / 2 >= 1);
	assert(nthreads % 2 == 0);

	for (size_t i = 0; i < nthreads; i++) {
		switch (i % 2) {
		case 0:
			if (what & INSERTS) {
				pthread_create(&threads[i], NULL, insert_thread, list);
			}
			break;
		case 1:
			if (what & DELETES) {
				pthread_create(&threads[i], NULL, delete_thread, list);
			}
			break;
		default:
			assert(0);
		}
	}

	for (size_t i = 0; i < nthreads; i++) {
		switch (i % 2) {
		case 0:
			if (what & INSERTS) {
				pthread_join(threads[i], NULL);
			}
			break;
		case 1:
			if (what & DELETES) {
				pthread_join(threads[i], NULL);
			}
			break;
		default:
			assert(0);
		}
	}
}

__attribute__((unused)) static void
clean_and_check(ll_list_t *list) {
	for (size_t itid = 0; itid < NTHREADS / 2; itid++) {
		for (size_t i = 0; i < NELEMENTS; i++) {
			if (ll_list_delete(list, (uintptr_t)&elements[itid][i])) {
				atomic_fetch_sub(&elements[itid][i], 1);
				(void)atomic_fetch_add(&deletes, 1);
			}
			if (atomic_load(&elements[itid][i]) != 0) {
				fprintf(stderr, "Found stray %zu inserts at elements[%zu][%zu]\n", (size_t)atomic_load(&elements[itid][i]), (size_t)itid, (size_t)i);
			}
		}
	}
}

int
main(void) {
	ll_list_t *list = ll_list_new();

	ll_list_insert(list, 1);
	ll_list_delete(list, 1);

	run_threads(list, NTHREADS, INSERTS);
	/* run_threads(list, NTHREADS, DELETES); */
	/* run_threads(list, NTHREADS, BOTH); */

	clean_and_check(list);

	ll_list_destroy(list);

	return (0);
}
