/*
 * Copyright (C) Internet Systems Consortium, Inc. ("ISC")
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * See the COPYRIGHT file distributed with this work for additional
 * information regarding copyright ownership.
 */

#include <inttypes.h>

typedef uintptr_t ll_key_t;
typedef struct ll_node ll_node_t;
typedef struct ll_list ll_list_t;

ll_list_t *
ll_list_new(void);
void
ll_list_destroy(ll_list_t *);
bool
ll_list_insert(ll_list_t *list, ll_key_t key);
bool
ll_list_delete(ll_list_t *list, ll_key_t key);
bool
ll_list_contains(ll_list_t *list, ll_key_t key);
