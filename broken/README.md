Forewarning: I am not a computer science theoretician, and I don't consider
myself to be a super star, so I could be completely wrong in my analysis.

This directory contain a work-in-progress implementation of:

* broken/list.c implements: Håkan Sundell and Philippas Tsigas. 2004. Lock-free
  and practical doubly linked list-based deques using single-word
  compare-and-swap. In Proceedings of the 8th international conference on
  Principles of Distributed Systems (OPODIS’04). Springer-Verlag, Berlin,
  Heidelberg, 240–255. DOI:https://doi.org/10.1007/11516798_18 (I used the
  revided and extended version of the paper that uses lastlink as a boolean, not
  as a link.)

* broken/pq.c implements: Håkan Sundell and Philippas Tsigas. 2005. Fast and
  lock-free concurrent priority queues for multi-thread systems. J. Parallel
  Distrib. Comput. 65, 5 (May 2005),
  609–627. DOI:https://doi.org/10.1016/j.jpdc.2004.12.005

The algorithms in this directory claim to use Valois-Michael-Scott memory
management scheme, but I was not able to apply the concept by modifying the
algorithm by returning the memory to the system allocator instead of keeping in
on the FreeList.  In fact, returning the memory to the allocator has an
advantage because tools like AddressSanitizer/ThreadSanitizer can be used for
correctness checks.

As a side note, the first paper has an error, the HI12 line needs to be:

    link1 := READ_DEL_NODE(node.prev);

instead of:

    link1 := node.prev;

for correct reference counting.

Still, even after this change, the algorithm just leaks memory at the end, so
either my implementation is completely wrong, or the algorithm makes wrong
assumptions about the underlying memory management.  Unfortunately, it's
impossible to use other memory management techniques as f.e. Hazard Pointers for
the algorithm, because HelpInsert() and HelpDelete() functions are called
recursively.
