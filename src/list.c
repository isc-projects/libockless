/*
 * Copyright (C) Internet Systems Consortium, Inc. ("ISC")
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * See the COPYRIGHT file distributed with this work for additional
 * information regarding copyright ownership.
 */

/*
 * List implementation using Michal-Harris lock-free list algorithm with Hazard Pointers.
 *
 * Maged M. Michael. 2002. High performance dynamic lock-free hash tables and list-based sets. In Proceedings of the
 * fourteenth annual ACM symposium on Parallel algorithms and architectures (SPAA ’02). Association for Computing
 * Machinery, New York, NY, USA, 73–82. DOI:https://doi.org/10.1145/564870.564881
 *
 * This code was cross-checked with C++ code from:
 * https://github.com/pramalhe/OneFile/blob/master/datastructures/linkedlists/HazardPointers.hpp
 *
 ******************************************************************************
 * Copyright (c) 2014-2017, Pedro Ramalhete, Andreia Correia
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Concurrency Freaks nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************
 */

#include <assert.h>
#include <stdalign.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <lockless/hp.h>
#include <lockless/list.h>

/*
 * PRIVATE
 */

#define HP_NEXT 0
#define HP_CURR 1
#define HP_PREV 2

#define ALIGNMENT 128

#define is_marked(p)	(bool)((uintptr_t)(p)&0x01)
#define get_marked(p)	((uintptr_t)(p) | (0x01))
#define get_unmarked(p) ((uintptr_t)(p) & (~0x01))

#define get_marked_node(p)   ((ll_node_t *)get_marked(p))
#define get_unmarked_node(p) ((ll_node_t *)get_unmarked(p))

struct ll_node {
	alignas(ALIGNMENT) uint32_t magic;
	alignas(ALIGNMENT) atomic_uintptr_t next;
	ll_key_t key;
};

struct ll_list {
	atomic_uintptr_t head; /* ll_node_t * */
	atomic_uintptr_t tail; /* ll_node_t * */
	ll_hp_t *hp;
};

ll_node_t *
ll_node_new(ll_key_t key) {
	ll_node_t *node = aligned_alloc(128, sizeof(*node));
	assert(node != NULL);
	*node = (ll_node_t){ .magic = 0xdeadbeaf, .key = key };
	return (node);
}

void
ll_node_destroy(ll_node_t *node) {
	if (node == NULL) {
		return;
	}
	assert(node->magic == 0xdeadbeaf);
	free(node);
}

static void
ll__list_node_delete(void *arg) {
	ll_node_t *node = (ll_node_t *)arg;
	ll_node_destroy(node);
}

static bool
ll__list_find(ll_list_t *list, ll_key_t *key, atomic_uintptr_t **par_prev, ll_node_t **par_curr, ll_node_t **par_next) {
	atomic_uintptr_t *prev = NULL;
	ll_node_t *curr = NULL, *next = NULL;

try_again:
	prev = &list->head;
	curr = (ll_node_t *)atomic_load(prev);
	(void)ll_hp_protect_ptr(list->hp, HP_CURR, (uintptr_t)curr);
	if (atomic_load(prev) != get_unmarked(curr)) {
		goto try_again;
	}
	while (true) {
		if (get_unmarked_node(curr) == NULL) {
			break;
		}
		next = (ll_node_t *)atomic_load(&get_unmarked_node(curr)->next);
		(void)ll_hp_protect_ptr(list->hp, HP_NEXT, get_unmarked(next));
		if (atomic_load(&get_unmarked_node(curr)->next) != (uintptr_t)next) {
			goto try_again;
		}
		if (get_unmarked(next) == atomic_load((atomic_uintptr_t *)&list->tail)) {
			break;
		}
		if (atomic_load(prev) != get_unmarked(curr)) {
			goto try_again;
		}
		if (get_unmarked_node(next) == next) {
			if (!(get_unmarked_node(curr)->key < *key)) {
				*par_curr = curr;
				*par_prev = prev;
				*par_next = next;
				return (get_unmarked_node(curr)->key == *key);
			}
			prev = &get_unmarked_node(curr)->next;
			(void)ll_hp_protect_release(list->hp, HP_PREV, get_unmarked(curr));
		} else {
			uintptr_t tmp = get_unmarked(curr);
			if (!atomic_compare_exchange_strong(prev, &tmp, get_unmarked(next))) {
				goto try_again;
			}
			ll_hp_retire(list->hp, get_unmarked(curr));
		}
		curr = next;
		(void)ll_hp_protect_release(list->hp, HP_CURR, get_unmarked(next));
	}
	*par_curr = curr;
	*par_prev = prev;
	*par_next = next;

	return false;
}

/* PUBLIC */

bool
ll_list_insert(ll_list_t *list, ll_key_t key) {
	ll_node_t *curr = NULL, *next = NULL;
	atomic_uintptr_t *prev = NULL;

	ll_node_t *node = ll_node_new(key);

	while (true) {
		if (ll__list_find(list, &key, &prev, &curr, &next)) {
			ll_node_destroy(node);
			ll_hp_clear(list->hp);
			return false;
		}
		atomic_store_explicit(&node->next, (uintptr_t)curr, memory_order_relaxed);
		uintptr_t tmp = get_unmarked(curr);
		if (atomic_compare_exchange_strong(prev, &tmp, (uintptr_t)node)) {
			ll_hp_clear(list->hp);
			return true;
		}
	}
}

bool
ll_list_delete(ll_list_t *list, ll_key_t key) {
	ll_node_t *curr, *next;
	atomic_uintptr_t *prev;

	while (true) {
		if (!ll__list_find(list, &key, &prev, &curr, &next)) {
			ll_hp_clear(list->hp);
			return false;
		}

		uintptr_t tmp = get_unmarked(next);

		if (!atomic_compare_exchange_strong(&curr->next, &tmp, get_marked(next))) {
			continue;
		}

		tmp = get_unmarked(curr);
		if (atomic_compare_exchange_strong(prev, &tmp, get_unmarked(next))) {
			ll_hp_clear(list->hp);
			ll_hp_retire(list->hp, get_unmarked(curr));
		} else {
			/* ll__list_find(list, &key, &prev, &curr, &next); */
			ll_hp_clear(list->hp);
		}
		return true;
	}
}

bool
ll_list_contains(ll_list_t *list, ll_key_t key) {
	ll_node_t *curr, *next;
	atomic_uintptr_t *prev;
	bool result = ll__list_find(list, &key, &prev, &curr, &next);
	ll_hp_clear(list->hp);
	return result;
}

ll_list_t *
ll_list_new(void) {
	ll_list_t *list = calloc(1, sizeof(*list));
	ll_node_t *head = ll_node_new(0);
	ll_node_t *tail = ll_node_new(0);
	ll_hp_t *hp = ll_hp_new(3, ll__list_node_delete);

	assert(list != NULL);
	assert(head != NULL);
	assert(tail != NULL);
	atomic_init(&head->next, (uintptr_t)tail);
	*list = (ll_list_t){ .hp = hp };
	atomic_init(&list->head, (uintptr_t)head);
	atomic_init(&list->tail, (uintptr_t)tail);

	return list;
}

void
ll_list_destroy(ll_list_t *list) {
	assert(list != NULL);
	ll_node_t *prev = (ll_node_t *)atomic_load(&list->head);
	ll_node_t *node = (ll_node_t *)atomic_load(&prev->next);
	while (node != NULL) {
		ll_node_destroy(prev);
		prev = node;
		node = (ll_node_t *)atomic_load(&prev->next);
	}
	ll_node_destroy(prev);
	ll_hp_destroy(list->hp);
	free(list);
}
