This is a C11 library implementing couple of basic lock-free algorithms.

As of this moment, only Michael-Harris lock-free list has been implemented, but
I will expand the algorithms and improve the library as we add more algorithms
to BIND 9.

The library requires C11 support in the compiler and Meson build system.

NOTE: Please ignore the git history, I was tired and I will clean this up before
it gets out of the hands.
